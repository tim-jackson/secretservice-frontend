import { Router } from '@reach/router';
import React from 'react';
import './App.css';

import { APP_ROUTES } from './app/routes';
import BaseLayout from './common/layouts/BaseLayout';
import LoginPage from './login/LoginPage';
import SecretPage from './secrets/SecretPage';
import EditSecretPage from './secrets/EditSecretPage';


const App = () => (
  <Router>
    <BaseLayout path={APP_ROUTES.ROOT}>
      <SecretPage path={APP_ROUTES.SECRETS} />
      <EditSecretPage path={APP_ROUTES.EDIT_SECRET} />
    </BaseLayout>
    <LoginPage path={APP_ROUTES.LOGIN} />
  </Router>
);

export default App;
