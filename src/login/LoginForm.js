import React from 'react';
import { Field, Form } from 'react-final-form';
import { Box, Flex } from 'rebass';

import Input from '../common/form/Input';
import SubmitButton from '../common/form/SubmitButton';
import SubmitError from '../common/form/SubmitError';

const LoginForm = ({ onSubmit }) => (
  <Form
    onSubmit={onSubmit}
    render={({ handleSubmit }) => (
      <form onSubmit={handleSubmit}>
        <Flex flexDirection="column">
          <SubmitError />
          <Field
            label="Username"
            name="username"
            type="text"
            component={Input}
          />
          <Field
            label="Password"
            name="password"
            type="password"
            component={Input}
            sx={{ letterSpacing: '1.5' }}
          />
          <Box>
            <SubmitButton bg="black">
              <Flex alignItems="center">
                Sign In
              </Flex>
            </SubmitButton>
          </Box>
        </Flex>
      </form>
    )}
  />
);

export default LoginForm;
