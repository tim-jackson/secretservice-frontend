import { inject, observer } from 'mobx-react';
import React, { Component } from 'react';
import {
  Flex, Text,
} from 'rebass';

import LoginForm from './LoginForm';


const LoginPage = inject(["authStore"])(observer(class LoginPage extends Component {
  render() {
    const { signIn } = this.props.authStore;
    return (
      <Flex sx={{ minHeight: '100vh' }}>
        <Flex width={3 / 4} p={36} flexDirection="column">
          <Flex
            justifyContent="center"
            width={1}
            sx={{ height: '100%' }}
            flexDirection="column"
            alignItems="center"
          >
            <Flex flexDirection="column" width={2 / 3}>
              <Text mb={100} fontSize={24}>
                Secret Service Login
              </Text>
              <LoginForm onSubmit={signIn} />
            </Flex>
          </Flex>
        </Flex>
      </Flex>
    );
  }
}));

export default LoginPage;
