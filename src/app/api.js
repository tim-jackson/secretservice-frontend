import axios from 'axios';
import { navigate } from '@reach/router';

import { APP_ROUTES } from './routes';

const axiosInstance = axios.create({
  baseURL: process.env.REACT_APP_API_URL,
  params: {},
});

export const getAccessToken = () => localStorage.getItem('token');

export const setToken = (data) => {
  localStorage.setItem('token', data.token);
};

export const clearToken = () => {
  localStorage.removeItem('token');
};

axiosInstance.interceptors.request.use(
  (config) => {
    const token = getAccessToken();
    if (token) {
      config.headers.Authorization = `Token ${token}`;
    }
    return config;
  },
  error => Promise.reject(error),
);

axiosInstance.interceptors.response.use(
  response => ({ ...response, data: response.data }),
  async (error) => {
    navigate(APP_ROUTES.LOGIN)
  },
);

export default axiosInstance;
