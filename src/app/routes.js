export const APP_ROUTES = {
  ROOT: '/',
  LOGIN: '/login',
  SECRETS: '/secrets',
  EDIT_SECRET: '/secrets/:secretId'
};

export const API_ROUTES = {
  ACCOUNT_TOKEN: '/account/token/',
  SECRETS: '/secret/',
};
