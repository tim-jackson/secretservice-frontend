import React from 'react';

import { Field, Form } from 'react-final-form';
import { Box, Flex } from 'rebass';

import SubmitButton from '../common/form/SubmitButton';
import SubmitError from '../common/form/SubmitError';
import Input from '../common/form/Input';

const AddSecret = ({ onSubmit }) => (
  <Form
    onSubmit={onSubmit}
    render={({ handleSubmit }) => (
      <form onSubmit={handleSubmit}>
        <Flex flexDirection="column">
          <SubmitError />
          <Field
            label="Add new Secret"
            name="secret_text"
            type="text"
            component={Input}
          />
          <Box>
            <SubmitButton bg="black">
              <Flex alignItems="center">
                Add Secret
              </Flex>
            </SubmitButton>
          </Box>
        </Flex>
      </form>
    )}
  />
);


export default AddSecret;
