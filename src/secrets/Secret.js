import React from 'react'

import { Flex, Text } from 'rebass';
import { Link } from '@reach/router'

const Secret = ({ secret, props }) => {
  return (
    <Flex color="black" width={1} pb={10}>
      <div>
          <div key={secret.id}>
            <div>
              <Text>{secret.secret_text}</Text> <Link to={`${secret.id}`}>(Edit)</Link>
            </div>
          </div>
      </div>
    </Flex>
  )
};

export default Secret;