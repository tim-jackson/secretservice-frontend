
import React, { Component } from 'react';
import {
  Box, Flex, Text, Heading
} from 'rebass';

import API from '../app/api';
import { API_ROUTES } from '../app/routes';
import SecretPane from './SecretPane';
import AddSecret from './AddSecret';


class SecretPage extends Component {
  constructor() {
    super();
    this.state = {
        secrets: [],
    };
  }

  componentDidMount() {
    API.get(API_ROUTES.SECRETS)
    .then((res) => {
      if (res) {
        this.setState({ secrets: res.data })
      }
    })
  }

  addSecret = (secret) => {
    API.post(API_ROUTES.SECRETS, {secret_text: secret.secret_text}).then((res) => {
      this.setState(state => {
        const secrets = [res.data, ...state.secrets];
        return {
          secrets,
        };
      });
    });
  }

  render() {
    return (
      <>
        <Heading px={32}>
            Secret Service
        </Heading>
        <Box px={32} pb={20}>
          <AddSecret onSubmit={this.addSecret} />
        </Box>
        <Text px={32} pb={10}>
          {this.state.secrets.length > 0 ? 'Your existing secrets are shown below:': 'You do not currently have any secrets'}
        </Text>
        <Flex>
            <SecretPane secrets={this.state.secrets} />
        </Flex>
      </>
    );
  }
}

export default SecretPage;
