
import React, { Component } from 'react';
import { navigate } from "@reach/router"
import {
  Box, Flex, Text, Heading
} from 'rebass';

import API from '../app/api';
import { APP_ROUTES, API_ROUTES } from '../app/routes';
import { Field, Form } from 'react-final-form';

import SubmitButton from '../common/form/SubmitButton';
import SubmitError from '../common/form/SubmitError';
import Input from '../common/form/Input';

class EditSecretPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
        secret_text: '',
    };
  }

  componentDidMount() {
    API.get(API_ROUTES.SECRETS + this.props.secretId + '/').then((res) => {
      this.setState(state => {
        return {'secret_text': res.data.secret_text}
      });
    });

  }

  editSecret = (secret) => {
    API.put(API_ROUTES.SECRETS + this.props.secretId + '/', {secret_text: secret.secret_text}).then((res) => {
      return navigate(APP_ROUTES.SECRETS)
    });
  }

  render() {
    return (
      <>
        <Box px={32}>
        <Heading>
            Secret Service
        </Heading>
        <Text pb={10}>
          The current secret text is: {this.state.secret_text}
        </Text>
        </Box>
        <Flex px={32}>
          <Form
            onSubmit={this.editSecret}
            render={({ handleSubmit }) => (
              <form onSubmit={handleSubmit}>
                <Flex flexDirection="column">
                  <SubmitError />
                  <Field
                    label="New secret text"
                    name="secret_text"
                    type="text"
                    component={Input}
                  />
                  <Box>
                    <SubmitButton bg="black">
                      <Flex alignItems="center">
                        Edit Secret
                      </Flex>
                    </SubmitButton>
                  </Box>
                </Flex>
              </form>
            )}
          />
        </Flex>
      </>
    );
  }
}

export default EditSecretPage;
