import React from 'react'

import { Flex } from 'rebass';

import Secret from './Secret';

const SecretPane = ({ secrets }) => {
  return (
    <Flex color="black" width={1} px={32}>
      <div>
        {secrets.map((secret) => (
          <Secret pb={10} secret={secret}/>
        ))}
      </div>
    </Flex>

  )
};

export default SecretPane;