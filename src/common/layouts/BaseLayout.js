
import React from 'react';
import { Flex } from 'rebass';

import styled from '@emotion/styled';


const Wrapper = styled(Flex)`
  padding-top: 20px;
`;



class BaseLayout extends React.Component {

  componentDidUpdate(prevProps) {
    if (prevProps.location.pathname !== this.props.location.pathname) {
      window.scrollTo({ top: 0 });
    }
  }

  render() {

    return (
      <Wrapper>
          { this.props.children }
      </Wrapper>
    );
  }
}

export default BaseLayout;
