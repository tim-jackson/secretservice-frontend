import React from 'react';
import { FormSpy } from 'react-final-form';
import { Button } from 'rebass';

const SubmitButton = ({ children, ...props }) => (
  <FormSpy>
    {({ submitting }) => (
      <Button {...props}>
        {children}
      </Button>
    )}
  </FormSpy>
);

export default SubmitButton;
