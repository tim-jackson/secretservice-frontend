import { navigate } from '@reach/router';
import API, { setToken, clearToken } from '../app/api';
import { API_ROUTES, APP_ROUTES } from '../app/routes';

export class AuthStore {
  signIn = async ({ username, password }) => {
    try {
      clearToken();
      const { data } = await API.post(API_ROUTES.ACCOUNT_TOKEN, {
        username,
        password,
      });
      setToken(data);
      navigate(APP_ROUTES.SECRETS);
    } catch (e) {
      clearToken();
    }
  };

  signOut = async () => {
    clearToken();
    navigate(APP_ROUTES.LOGIN);
  };
}


export default new AuthStore();
